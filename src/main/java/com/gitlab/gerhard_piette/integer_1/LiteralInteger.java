package com.gitlab.gerhard_piette.integer_1;

import com.gitlab.gerhard_piette.letter_1.DefectLetter;
import com.gitlab.gerhard_piette.text_6.Text;

public interface LiteralInteger {

	public boolean isUnderscore(int cp);

	public boolean isLetterBin(int cp);

	public boolean isLetterDec(int cp);

	public boolean isLetterOct(int cp);

	public boolean isLetterHex(int cp);

	/**
	 * @return The offset after the integer or -offset - 1.
	 */
	public int afterInteger(Text tt, int offset) throws DefectLetter;

	/**
	 * @return The offset after the integer or -offset - 1.
	 */
	public int afterIntegerWithUnderscore(Text tt, int offset) throws DefectLetter;

	/**
	 * @return The offset after the integer or -offset - 1.
	 */
	public Integer afterIntegerBin(Text tt, int offset) throws DefectLetter;

	/**
	 * @return The offset after the integer or -offset - 1.
	 */
	public Integer afterIntegerBinWithUnderscore(Text tt, int offset) throws DefectLetter;

	/**
	 * @return The offset after the integer or -offset - 1.
	 */
	public int afterIntegerOct(Text tt, int offset) throws DefectLetter;


	/**
	 * @return The offset after the integer or -offset - 1.
	 */
	public int afterIntegerOctWithUnderscore(Text tt, int offset) throws DefectLetter;

	/**
	 * @return The offset after the integer or -offset - 1.
	 */
	public int afterIntegerDec(Text tt, int offset) throws DefectLetter;

	/**
	 * @return The offset after the integer or -offset - 1.
	 */
	public int afterIntegerDecWithUnderscore(Text tt, int offset) throws DefectLetter;

	/**
	 * @return The offset after the integer or -offset - 1.
	 */
	public int afterIntegerHex(Text tt, int offset) throws DefectLetter;

	/**
	 * @return The offset after the integer or -offset - 1.
	 */
	public int afterIntegerHexWithUnderscore(Text tt, int offset) throws DefectLetter;

}
