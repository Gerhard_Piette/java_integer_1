package com.gitlab.gerhard_piette.integer_1;

import com.gitlab.gerhard_piette.ascii_1.Ascii;
import com.gitlab.gerhard_piette.defect_2.DefectOffset;
import com.gitlab.gerhard_piette.letter_1.DefectLetter;
import com.gitlab.gerhard_piette.text_6.Text;

public class UnicodeInteger implements LiteralInteger {

	public boolean isUnderscore(int cp) {
		if (cp == Ascii.Underscore) {
			return true;
		}
		return false;
	}

	public boolean isLetterBin(int cp) {
		if (cp < Ascii.Digit_0 || cp > Ascii.Digit_1) {
			return false;
		}
		return true;
	}

	public boolean isLetterDec(int cp) {
		if (cp < Ascii.Digit_0 || cp > Ascii.Digit_9) {
			return false;
		}
		return true;
	}

	public boolean isLetterOct(int cp) {
		if (cp < Ascii.Digit_0 || cp > Ascii.Digit_7) {
			return false;
		}
		return true;
	}

	public boolean isLetterHex(int cp) {
		if (isLetterDec(cp)) {
			return true;
		}
		if (cp >= Ascii.Capital_A && cp <= Ascii.Capital_F) {
			return true;
		}
		if (cp >= Ascii.Small_A && cp <= Ascii.Small_F) {
			return true;
		}
		return false;
	}

	public int afterInteger(Text tt, int offset) throws DefectLetter {
		var ret = afterIntegerBin(tt, offset);
		if (ret > 0) {
			return ret;
		}
		ret = afterIntegerOct(tt, offset);
		if (ret > 0) {
			return ret;
		}
		ret = afterIntegerDec(tt, offset);
		if (ret > 0) {
			return ret;
		}
		ret = afterIntegerHex(tt, offset);
		return ret;
	}

	public int afterIntegerWithUnderscore(Text tt, int offset) throws DefectLetter {
		var ret = afterIntegerBinWithUnderscore(tt, offset);
		if (ret > 0) {
			return ret;
		}
		ret = afterIntegerOctWithUnderscore(tt, offset);
		if (ret > 0) {
			return ret;
		}
		ret = afterIntegerDecWithUnderscore(tt, offset);
		if (ret > 0) {
			return ret;
		}
		ret = afterIntegerHexWithUnderscore(tt, offset);
		return ret;
	}

	/**
	 * @return The offset after the integer or -1.
	 */
	public Integer afterIntegerBin(Text tt, int offset) throws DefectLetter {
		try {
			var off = offset;
			// first digit
			var le = tt.read(off);
			if (!isLetterBin(le.letter)) {
				return -offset - 1;
			}
			off = le.end;
			// other digits
			while (off < tt.getLength()) {
				le = tt.read(off);
				if (!isLetterBin(le.letter)) {
					break;
				}
				off = le.end;
			}
			return off;
		} catch (DefectOffset defect) {
			return -offset - 1;
		}
	}


	/**
	 * @return The offset after the integer or -1.
	 */
	public Integer afterIntegerBinWithUnderscore(Text tt, int offset) throws DefectLetter {
		try {
			var off = offset;
			// first digit
			var le = tt.read(off);
			if (!isLetterBin(le.letter)) {
				return -offset - 1;
			}
			off = le.end;
			// other digits
			while (off < tt.getLength()) {
				le = tt.read(off);
				if (!isLetterBin(le.letter) && !isUnderscore(le.letter)) {
					break;
				}
				off = le.end;
			}
			return off;
		} catch (DefectOffset defect) {
			return -offset - 1;
		}
	}


	/**
	 * @return The offset after the integer or -1.
	 */
	public int afterIntegerOct(Text tt, int offset) throws DefectLetter {
		try {
			var off = offset;
			// first digit
			var le = tt.read(off);
			if (!isLetterOct(le.letter)) {
				return -offset - 1;
			}
			off = le.end;
			// other digits
			while (off < tt.getLength()) {
				le = tt.read(off);
				if (!isLetterOct(le.letter)) {
					break;
				}
				off = le.end;
			}
			return off;
		} catch (DefectOffset defect) {
			return -offset - 1;
		}
	}


	/**
	 * @return The offset after the integer or -1.
	 */
	public int afterIntegerOctWithUnderscore(Text tt, int offset) throws DefectLetter {
		try {
			var off = offset;
			// first digit
			var le = tt.read(off);
			if (!isLetterOct(le.letter)) {
				return -offset - 1;
			}
			off = le.end;
			// other digits
			while (off < tt.getLength()) {
				le = tt.read(off);
				if (!isLetterOct(le.letter) && !isUnderscore(le.letter)) {
					break;
				}
				off = le.end;
			}
			return off;
		} catch (DefectOffset defect) {
			return -offset - 1;
		}
	}

	/**
	 * @return The offset after the integer or -1.
	 */
	public int afterIntegerDec(Text tt, int offset) throws DefectLetter {
		try {
			var off = offset;
			// first digit
			var le = tt.read(off);
			if (!isLetterDec(le.letter)) {
				return -offset - 1;
			}
			off = le.end;
			// other digits
			while (off < tt.getLength()) {
				le = tt.read(off);
				if (!isLetterDec(le.letter)) {
					break;
				}
				off = le.end;
			}
			return off;
		} catch (DefectOffset defect) {
			return -offset - 1;
		}
	}

	/**
	 * @return The offset after the integer or -1.
	 */
	public int afterIntegerDecWithUnderscore(Text tt, int offset) throws DefectLetter {
		try {
			var off = offset;
			// first digit
			var le = tt.read(off);
			if (!isLetterDec(le.letter)) {
				return -offset - 1;
			}
			off = le.end;
			// other digits
			while (off < tt.getLength()) {
				le = tt.read(off);
				if (!isLetterDec(le.letter) && !isUnderscore(le.letter)) {
					break;
				}
				off = le.end;
			}
			return off;
		} catch (DefectOffset defect) {
			return -offset - 1;
		}
	}


	public int afterIntegerHex(Text tt, int offset) throws DefectLetter {
		try {
			var off = offset;
			// first digit
			var le = tt.read(off);
			if (!isLetterHex(le.letter)) {
				return -offset - 1;
			}
			off = le.end;
			// other digits
			while (off < tt.getLength()) {
				le = tt.read(off);
				if (!isLetterHex(le.letter)) {
					break;
				}
				off = le.end;
			}
			return off;
		} catch (DefectOffset defect) {
			return -offset - 1;
		}
	}

	public int afterIntegerHexWithUnderscore(Text tt, int offset) throws DefectLetter {
		try {
			var off = offset;
			// first digit
			var le = tt.read(off);
			if (!isLetterHex(le.letter)) {
				return -offset - 1;
			}
			off = le.end;
			// other digits
			while (off < tt.getLength()) {
				le = tt.read(off);
				if (!isLetterHex(le.letter) && !isUnderscore(le.letter)) {
					break;
				}
				off = le.end;
			}
			return off;
		} catch (DefectOffset defect) {
			return -offset - 1;
		}
	}

}
